Composition of bacterial, fungal and collembola community structure by Illumina MiSeq Sequencing
================================================================================================

The outcome of this practical will be the raw DNA sequences of taxonomic markers to reconstruct the Composition of bacterial, fungal and collembola community structure on the samples collected in the fields.
For bacteria and fungi we will use metagenomic DNA (mgDNA), conversely for collembola environmental DNA (eDNA) will be investigated


**Activity**

mgDNA and eDNA will be extracted from samples of soil, cryoconite and water. Bacterial 16S rDNA and fungal ITS1 will be amplified by PCR from gDNA. Marker genes for collembola (16S mitochondrial rDNA) will be amplified from eDNA. 
DNA libraries will be prepared and a single run of MiSeq Illumina sequencing will be carried out.


**Tutors/trainers**
Ewa Poniecka, Francesca Pittino, Andrea Franzetti, Krzysztof Zawierucha , Alessia Guerrieri, Francesca Formicola, Valeria Tatangelo




