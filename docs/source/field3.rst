Identification of plants, ground beetles, springtails, chironomid and tardigrades
=================================================================================


**Activity**

✦ Plants are the trophic and structural base of the ecological succession. We will analyze them under stereomicroscope and light microscope in order to observe their morphological features and then, to identify them.

✦ Carabid beetles are among the most abundant cold-adapted ground-dwelling arthropods colonizing the glacier forelands. We will analyze them under stereomicroscope in order to observe their morphological features and then, to identify them to genus level.

✦ Collembola are a dominant  animal taxon colonising the glacier surface. We will analyze them under stereomicroscope and light microscope in order to observe their morphological features and then, to identify them at the lowest possible level.

✦ Non-biting midges (Diptera Chironomidae) are among the most abundant cold-adapted freshwater insects colonizing glacier-fed streams. We will analyze them under stereomicroscope in order to observe their morphological features and then, to identify them to genus level.

✦ Tardigrades are dominant faunal group in cryoconite holes on alpine glaciers and one of the most abundant microscopic animals in moss and soil habitats. We will analyze morphological differences between tardigrades from glacier and glacier adjacent habitats under stereo-microscopes. The training will also include preparation of slides and identification of animals to basic trophic groups under phase contrast microscopes.

**Tutors/trainers**
Mauro Gobbi, Valeria Lencioni, Krzysztof Zawierucha, Marco Caccianiga, Barbara Valle


