Sampling design and sample collection for microbial biodiversity and eDNA
=========================================================================

**Activity**

Participants choose a hypothesis regarding microbial biodiversity and its controls in the glacial ecosystem (e.g. comparison of big vs small cryoconite holes; microbial community of ice vs cryoconite holes vs proglacial soils; different times of day; community dynamics along a chronosequence etc.) and design a sampling scheme to cover their research question. They’ll learn to choose/invent hypothesis which can be answered in a short time (mimicking pilot studies in real-life scenario), to plan sampling in a limited time, to select minimum and maximum plan and adjust their plans to weather conditions, to divide work between group members. 

The activity will consist of sample collection: i.) on a glacier – cryoconite sediment, stream water etc. ii.) in the forefield. The collected samples will include a material for DNA barcoding, samples for total cell counts, samples for organic matter content. After collection samples will need to be frozen until analysis in the lab. 


**Outcome**
Samples for molecular analyses will be collected


**Tutors/trainers**
Ewa Poniecka, Francesca Pittino, Andrea Franzetti, Roberto Ambrosini, Alessia Guerrieri
