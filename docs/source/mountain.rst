Practicals at the Stelvio National Park
=======================================

**Rifugio Branca - The mountain venue**

The mountain venue of the course is the Branca shelter (Rifugio Branca https://www.rifugiobranca.it/Home-en) in Santa Caterina Valfurva (SO), Italy. It is the closet access to the Forni Glacier and its forefield which is the study area of the course. 
The Branca mountain shelter is located 2,943 mts. above sea level in the heart of the Stelvio National Park and is run by the Alberti/Confortola family, situated facing the large Forni Glacier and dominated by the S.Matteo (3678 m.), Tresero, (3594 m.), Vioz (3645 m.), Palòn de la Mare (3708 m.) and Cevedale (3769 m.) peaks. 
It offers one of the rarest panoramic views of the Alps and is the departure point for numerous ascents to all the peaks in the Forni Glacier Circle.
We planned two field activities on both the surface of the Forni Glacier and in the forefield, some lectures and a practical activity.
Safety on the field will be assured by professional Alpine guides during all outdoor activities.

Contents:

.. toctree::
   :maxdepth: 2

   field1
   field2
   field3






