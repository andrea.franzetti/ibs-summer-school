Plant, ground beetle, springtail, tardigrades and chironomid assemblages along the Forni Glacier foreland and on its surface
============================================================================================================================

**Activity**

We will investigate i) plant succession along the chronosequence of glacier retreat, through floristic survey and dendrochronology, ii) ground beetle (Coleoptera: Carabidae) succession along the chronosequence of glacier retreat and on its tongue through the most commonly used sampling methods, iii) the distribution and the ecology of a strictly cryophilic taxon of springtails (Collembola), glacial Isotomidae, iv) chironomids (Diptera Chironomidae) distribution in the glacier-fed streams and on the glacier through the most commonly used sampling methods, v) diversity and  physiological responses and survival of (1) glacier tardigrades (Tardigrada) to high temperature and desiccation in the forefield, (2) moss tardigrades to low temperatures of cryoconite holes.

**Tutors/trainers**
Mauro Gobbi, Valeria Lencioni, Krzysztof Zawierucha, Marco Caccianiga, Barbara Valle


