EMBO Practical Course "Integrating traditional and molecular approaches in ecology of glacial habitats (ICME11)"
================================================================================================================

Welcome to the **EMBO Practical Course "Integrating traditional and molecular approaches in ecology of glacial habitats**

**2-6 August 2022 Santa Caterina Valfurva (SO) Italy - Stelvio National Park, Rifugio C. Branca Ortles-Cevedale group 2493 m asl** `www.rifugiobranca.it`_.

.. _www.rifugiobranca.it: http://www.rifugiobranca.it/Home-en

**7-13 August 2022 Milano (MI) Italy - Dept. of Erath and Environmental Sciences - University of Milano Bicocca** `www.disat.unimib.it`_.

.. _www.disat.unimib.it: http://www.disat.unimib.it/en

The focus of the course is to provide the young generation of scientists with all the knowledge and skills to integrate traditional morphologic and molecular approaches in order to design and execute a "cradle-to-grave" investigation of glacier ecology at multiple trophic levels

This documentations describes the practical activities on both the venues and provides instrcutions for computer practicals. 


Contents:

.. toctree::
   :maxdepth: 2
   
   mountain
   bicocca 

