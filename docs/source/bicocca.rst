Practicals at the University of Milano Bicocca
==========================================

**From the samples to the manuscript**

The goal of the practical activities at the University of Milano Bicocca is to follow the whole scientific process that goes from the samples analysis to determine the biological communuty structure, to the bioinformatic and statistical elaboration of the data obtained from both traditional and molecular methods.
Results will be then discussed and a scientific manuscript will be drafted. The follow up of the course will be a manuscrupt with the findings of the scientific activities co-authored by the instructors and by the participants that want to collaborate.



Contents:

.. toctree::
   :maxdepth: 2

   wet1
   informatics
   paper
   
   
   
   






