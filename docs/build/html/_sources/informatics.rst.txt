Bioinformatics and statistics (Univ. Milano Bicocca)
====================================================

**Activity**

The raw sequences obtained by Illumina MiSeq will be elaborated by biojformatics and statistical tools.

**Access to the Virtual Machine**

Open a browser and go::

	https://libaas-lessons.si.unimib.it/

Access with your personal credential

Contents:

.. toctree::
   :maxdepth: 2

   bioinf
   stat






