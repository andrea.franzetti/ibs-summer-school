Amplicon Sequence Variants (ASVs)
=================================



**Define ASVs and their abundance**

Move to data folder::

      cd $HOME/MEM(MA)_practical/amplicons


Move _1.fastq file to R1 folder and _2.fastq to folder R2::

     cp *_1.fastq /Microbiology/amplicons/dada2/R1
     cp *_2.fastq /MIcrobiology/amplicons/dada2/R2


MOve to the dada2 directory and lunch the R script::

     cd /Microbiology/amplicons/dada2/
     Rscript  .dada.script/dada2_180719_big.data_wd.R


