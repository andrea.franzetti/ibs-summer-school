#!/bin/bash


#Script per pipeline "Analisi ampliconi 16S da Illumina" ver 13/10/2015.
#Analisi basate su usearch v8

read -p "Inserisci il nome del progetto " pr

read -p "Inserisci la lista dei campioni " list

read -p "Premi 'invio' per merging e quality filtering " ans


echo "Eseguiamo merging, quality filtering...." 

for i in $list; 

do


echo "Riduciamo la lunghezza delle reads e eseguiamo merging, quality filtering...."

python3 ./bin/split_fastq.py ${i}.R1.fastq 160
python3 ./bin/split_fastq.py ${i}.R2.fastq 160

./bin/usearch8 -fastq_mergepairs cut.${i}.R1.fastq -reverse cut.${i}.R2.fastq -fastaout filtered.merged.$i.fasta -fastq_maxdiffs 0  -fastq_merge_maxee 0.5

done

read -p "Osserva i files che sono stati generati (filtered.merged*), poi premi 'invio' per proseguire" ans


echo "Concatena i file di sequenze..."
cat filtered.merged.*.fasta > $pr.cat.fasta

read -p "Osserva il file che è stato generato (*cat.fasta), poi premi 'invio' per proseguire" ans

echo "Eseguiamo dereplication, size sorting e clustering..."
./bin/usearch8 -derep_fulllength $pr.cat.fasta -fastaout uniques.ns.$pr.cat.fasta -sizeout -minuniquesize 2
./bin/usearch8 -cluster_otus uniques.ns.$pr.cat.fasta -otus otus.$pr.fasta -uparseout out.up -relabel OTU_ -sizein -sizeout

sed 's/;.*;//g' otus.$pr.fasta > renamed.otus.$pr.fasta 
#python /usr/local/bin/python_scripts/fasta_number.py otus.$pr.fasta OTU_ > renamed.otus.$pr.fasta

read -p "Osserva i files che sono stati generati, poi premi 'invio' per proseguire" ans

echo "Mappiamo le sequenze di ciascun file verso le sequenze rappresentative delle OTU..."

for i in $list ;

do
./bin/usearch8 -usearch_global filtered.merged.$i.fasta -db renamed.otus.$pr.fasta -id 0.97 -strand plus -userout mapping.$i.tg -userfields target

done

read -p "Osserva i files che sono stati generati, poi premi 'invio' per proseguire" ans

echo "Contiamo il numero di sequenze mappanti..."
wc -l mapping*tg

read -p "Inserisci il numero di sequenze mappanti da selezionare: " num

echo "Estraiamo un subset di sequenze mappate e formattiamo i files..."

for i in $list;

do

shuf -n $num mapping.$i.tg > sub.mapping.$i.tg
cut -f1 -d ';' sub.mapping.$i.tg > cut.sub.mapping.$i.tg
sed "1i $i" cut.sub.mapping.$i.tg > hd.cut.sub.mapping.$i.tg

done

paste hd.cut.sub.mapping* > hd.$pr.mapping.txt

read -p "Osserva il file hd*mapping.txt che è stato generato, poi premi 'invio' per proseguire" ans

echo "Classifichiamo le sequenze rappresentative delle OTU con RDP...."

java -Xmx1g -jar ./rdp_classifier_2.11/dist/classifier.jar -c 0.8 -o $pr.otu.rdp.txt -f fixrank  renamed.otus.$pr.fasta

read -p "Osserva il file *.rdp.txt che è stato generato, poi premi 'invio' per proseguire" ans

echo "Eseguiamo il merging delle tabelle ottenute..."

python3 ./bin/count_illumina.py hd.$pr.mapping.txt $pr.otu.rdp.txt $num




