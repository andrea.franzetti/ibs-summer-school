#!/usr/bin/python

"""

Usage:
python split_fastq.py [fastq file] [length]


"""
    
import io
import sys
import string
import numpy
from Bio import SeqIO
from Bio import Seq



if len(sys.argv) == 1:
	print (__doc__)
	exit(0)

      
fastq = open(sys.argv[1], "r")

file1=SeqIO.parse(fastq, "fastq")



for seq in file1:
	l=int(sys.argv[2])
	seq_t=seq[:l]
	out=open('cut.'+ sys.argv[1],'a')
	SeqIO.write(seq_t, out, "fastq")
