#!/usr/bin/python

"""
Usage:
count.illumina.py [arg1] [arg2]
[arg1]: file mapping prodotto dallo script Illumina
[arg2]: file rdp prodotto dalle script Illumina
....
"""
    
import io
import sys
import string
import pandas as pd
import numpy


if len(sys.argv) == 1:
	print (__doc__)
	exit(0)


data=pd.read_table(sys.argv[1])
count=data.apply(pd.value_counts).fillna(0)
count.to_csv(open('table.'+sys.argv[1],'w'),sep='\t')
table=pd.read_table('table.'+sys.argv[1])
table.rename(columns={'Unnamed: 0':'otu'}, inplace=True)



names=['otu','blank', 'Domain', 'Rank1', 'Domain_c', 'Phylum', 'Rank2', 'Phylum_c', 'Class', 'Rank3', 'Class_c', 'Order', 'Rank4', 'Order_c', 'Family', 'Rank5', 'Family_c', 'Genus', 'Rank6', 'Genus_c']
rdp=pd.read_table(sys.argv[2],header=None,names=names)


merged=pd.merge(table,rdp,how='left',on='otu')

merged.to_csv(open('merged.'+sys.argv[1]+'.rdp.txt','w'),sep='\t')

